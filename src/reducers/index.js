import { combineReducers } from 'redux';
import caseReducer from './caseReducer';
import authReducer from './auth-reducer';
import workflowReducer from './workflowReducer';
import scenarioReducer from './scenarioReducer';
import proxiesReducer from "./proxiesReducer"

export default combineReducers({
    cases: caseReducer,
    auth: authReducer,
    workflowReducer: workflowReducer,
    scenarioReducer: scenarioReducer,
    proxiesReducer: proxiesReducer
});
