import {
    GET_ALL_SCENARIOS,
    GET_ONE_SCENARIO,
    GET_REPORTS,
    GET_ONE_REPORT
} from "../actions/types"

const intialState = {
    scenarios: [],
    scenario: {},
    report:{},
    report_one: {}
}

export default function(state = intialState, action) {
    switch (action.type) {
        case GET_ALL_SCENARIOS:
            return {
                ...state,
                scenarios: action.payload
            }
        case GET_ONE_SCENARIO:
            return {
                ...state,
                scenario: action.payload
            }
        case GET_REPORTS:
            return {
                ...state,
                report: action.payload
            }
        case GET_ONE_REPORT:
            return {
                ...state,
                report_one: action.payload
            }
        default:
            return state
    }
}