import { LOGIN_SET_ME, LOGIN_SET_TOKEN, LOGIN_SET_ERROR } from '../actions/types'

const initialState = {
    me: null,
    token: '',
    error: {}
}

export default function(state = initialState, action) {
    switch (action.type) {
        case LOGIN_SET_ME:
            return {
                ...state,
                me: action.payload,
                error: ''
            }
        case LOGIN_SET_TOKEN:
            return {
                ...state,
                token: action.payload,
                error: ''
            }
        case LOGIN_SET_ERROR:
            return {
                ...state,
                token: '',
                error: action.payload
            }
        default:
            return state;
    }
}