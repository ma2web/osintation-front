import { FETCH_CASES, NEW_CASE } from '../actions/types'

const initialState = {
    items: [],
    item: {}
}

export default function(state = initialState, action) {
    switch (action.type) {
        case FETCH_CASES:
            return {
                ...state,
                items: action.payload
            }
        case NEW_CASE:
            return {
                ...state,
                item: action.payload
            }
        default:
            return state;
    }
}