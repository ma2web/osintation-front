import {
    GET_ALL_WORKFLOWS,
    GET_ONE_WORKFLOW
} from "../actions/types"

const intialState = {
    workflows: [],
    workflow: {}
}

export default function(state = intialState, action) {
    switch (action.type) {
        case GET_ALL_WORKFLOWS:
            return {
                ...state,
                workflows: action.payload
            }
        case GET_ONE_WORKFLOW:
            return {
                ...state,
                workflow: action.payload
            }
        default:
            return state
    }
}