import { GET_ALL_PROXIES } from '../actions/types';

const initialState = {
    proxies: []
}

export default function(state = initialState, action) {
    switch (action.type) {
        case GET_ALL_PROXIES:
            return {
                ...state,
                proxies: action.payload
            }
        default:
            return state
    }
}
