import React, { Component } from 'react';
import { Button, Row, Col } from 'reactstrap';
import { TiTrash, TiEye, TiMediaPlayOutline, TiMediaStopOutline } from 'react-icons/ti';
import { Loader } from '../../../vibe';
import { getAllProxies } from '../../../actions/proxiesActions';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';

class AssignedWorkflows extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
        };
    }
    componentDidMount() {
        this.props.getAllProxies().then(() => {
            this.setState({
                loading: false
            })
        })
    }
    render() {
        return (
            <React.Fragment>
                {!this.state.loading ? (
                    Array.isArray(this.props.proxies) && this.props.proxies.length ? (
                        <Row>
                            <Col className="col-sm-12">
                                <table className="osint-table proxies-table">
                                    <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th>Type</th>
                                            <th>Host</th>
                                            <th>Port</th>
                                            <th>Raw Proxy</th>
                                            <th>Created at</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.props.proxies.map(item => {
                                            const date = new Date(item.created_at.$date);
                                            return (
                                                <tr key={item.id}>
                                                    <th scope="row"><div className="active-proxy"></div></th>
                                                    <th scope="row">{item.type}</th>
                                                    <th scope="row">{item.host}</th>
                                                    <th scope="row">{item.port}</th>
                                                    <th scope="row">{item.raw_proxy}</th>
                                                    <td>{date.toLocaleDateString()}</td>
                                                </tr>
                                            );
                                        })}
                                    </tbody>
                                </table>
                            </Col>
                        </Row>
                    ) : (
                        <Col className="nothing-have-to-show">
                            <p>No workflow!</p>
                        </Col>
                    )
                ) : (
                    <Col className="loader-container">
                        <Loader type="puff" />
                    </Col>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    proxies: state.proxiesReducer.proxies,
});

export default connect(mapStateToProps, { getAllProxies })(AssignedWorkflows);
