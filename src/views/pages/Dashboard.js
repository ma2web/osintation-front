import React, { Component } from 'react';
import reactFeature from '../../assets/images/react-feature.svg';
import sassFeature from '../../assets/images/sass-feature.svg';
import bootstrapFeature from '../../assets/images/bootstrap-feature.svg';
import responsiveFeature from '../../assets/images/responsive-feature.svg';
import { Card, CardBody, Row, Col } from 'reactstrap';
import Particles from './Particles';
import Logo from '../../assets/images/logo-white.png';
import rasalLogo from '../../assets/images/rasaLab.png';

class Dashboard extends Component {
    render() {
        const heroStyles = {
            padding: '50px 0 70px',
        };

        return (
            <div>
                <Row>
                    <Col md={12}>
                        <Particles />
                    </Col>
                    <Col md={12}>
                        <div className="home-hero" style={heroStyles}>
                            <Row style={{ alignItems: 'center' }}>
                                <Col md={2}>
                                    <img src={Logo} width={200} />
                                </Col>
                                <Col md={6}>
                                    <h1 className="align-middle" style={{ color: '#fff', fontSize: 50 }}>
                                        Welcome to{' '}
                                        <span style={{ color: '#ff7731' }}>Osintation!</span>
                                    </h1>
                                    <p className="text-muted">
                                        Osintation combines the creative exploration capabilities of
                                        humans with the automation potential of machines. Osintation
                                        is a much loved and widely used tool for open source
                                        intelligence and graphical link analyses.
                                    </p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                    <div className="rasa-logo">
                                  <img src={rasalLogo} />
                                </div>
                </Row>
                {/* <Row>
          <Col md={6}>
            <Card>
              <CardBody className="display-flex">
                <img
                  src={reactFeature}
                  style={{ width: 70, height: 70 }}
                  alt="react.js"
                  aria-hidden={true}
                />
                <div className="m-l">
                  <h2 className="h4">React.js</h2>
                  <p className="text-muted">
                    Built to quickly get your MVPs off the ground.
                  </p>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col md={6}>
            <Card>
              <CardBody className="display-flex">
                <img
                  src={bootstrapFeature}
                  style={{ width: 70, height: 70 }}
                  alt="Bootstrap"
                  aria-hidden={true}
                />
                <div className="m-l">
                  <h2 className="h4">Bootstrap 4</h2>
                  <p className="text-muted">
                    The most popular framework to get your layouts built.
                  </p>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <Card>
              <CardBody className="display-flex">
                <img
                  src={sassFeature}
                  style={{ width: 70, height: 70 }}
                  alt="Sass"
                  aria-hidden={true}
                />
                <div className="m-l">
                  <h2 className="h4">Sass</h2>
                  <p className="text-muted">
                    Easily change the design system styles to fit your needs.
                  </p>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col md={6}>
            <Card>
              <CardBody className="display-flex">
                <img
                  src={responsiveFeature}
                  style={{ width: 70, height: 70 }}
                  alt="Responsive"
                  aria-hidden={true}
                />
                <div className="m-l">
                  <h2 className="h4">Responsive</h2>
                  <p className="text-muted">
                    Designed for screens of all sizes.
                  </p>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row> */}
            </div>
        );
    }
}

export default Dashboard;
