import React, { Component } from 'react';
import {
    Form,
    Label,
    Input,
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    FormGroup,
} from 'reactstrap';
import { TiArrowRepeat } from 'react-icons/ti';
import { toast } from 'react-toastify';

export class UpdateScenario extends Component {
    constructor(props) {
        super(props);
        this.state = {
            _id: '',
            scenario: {
                scenario_name: '',
                description: '',
            },
        };
    }
    componentWillReceiveProps() {
        this.setState({
            _id: this.props.scenarioId,
            scenario: {
                ...this.state.scenario,
                scenario_name: this.props.scenarioName,
                description: this.props.scenarioDescription,
            },
        });
    }
    onChange = e => {
        this.setState({
            scenario: {
                ...this.state.scenario,
                [e.target.name]: e.target.value,
            },
        });
    };
    onSubmit = e => {
        e.preventDefault();
        this.props.updateScenario(this.state).then(res => {
            if (res.status === 'OK') {
                this.props.getOneScenario(this.props.scenarioId).then(() => {
                    toast(`Scenario has been updated successfully.`, { type: 'info' });
                    this.props.close(e);
                });
            } else {
                toast(res.error, { type: 'error' });
            }
        });
    };
    render() {
        return (
            <Modal isOpen={this.props.isOpen} toggle={e => this.props.close(e)}>
                <Form onSubmit={this.onSubmit}>
                    <ModalHeader toggle={e => this.props.close(e)}>
                        <h3>Edit Scenario</h3>
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="scenario_name">Name</Label>
                            <Input
                                type="text"
                                id="scenario_name"
                                name="scenario_name"
                                value={this.state.scenario.scenario_name}
                                onChange={this.onChange}
                                required
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="description">Description</Label>
                            <Input
                                type="textarea"
                                name="description"
                                id="description"
                                value={this.state.scenario.description}
                                onChange={this.onChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <small>id: </small>
                            <code>{this.props.scenarioId}</code>
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" type="submit">
                            <TiArrowRepeat /> update
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
        );
    }
}

export default UpdateScenario;
