import React, { Component } from 'react';
import { Form, Button, Modal, ModalHeader, ModalBody, ModalFooter, Alert } from 'reactstrap';
import { TiPlusOutline, TiTimesOutline } from 'react-icons/ti';
import { connect } from 'react-redux';
import { deleteScenario, getAllScenarios } from '../../../actions/scenarioActions';
import { toast } from 'react-toastify';

export class CreateScenario extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scenario_name: '',
            description: '',
        };
    }
    onChange = e => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };
    onSubmit = e => {
        e.preventDefault();
        let id = {
            _id: this.props.id,
        };
        this.props.deleteScenario(id).then(res => {
            if (res.status === 'OK') {
                this.props.getAllScenarios().then(() => {
                    toast(`Scenario has been deleted successfully.`, { type: 'info' });
                    this.props.close(e);
                });
            } else {
                toast(res.error, { type: 'error' });
            }
        });
    };
    render() {
        return (
            <Modal isOpen={this.props.isOpen} toggle={e => this.props.close(e)}>
                <Form onSubmit={this.onSubmit}>
                    <ModalHeader toggle={e => this.props.close(e)}>
                        <h3>Delete Scenario</h3>
                    </ModalHeader>
                    <ModalBody>
                        <Alert color="danger">
                            You are trying to Delete Scenario!
                        </Alert>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" type="submit">
                            <TiPlusOutline /> Delete
                        </Button>{' '}
                        <Button color="warning" onClick={e => this.props.close(e)}>
                            <TiTimesOutline /> Cancel
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
        );
    }
}

export default connect(null, { deleteScenario, getAllScenarios })(CreateScenario);
