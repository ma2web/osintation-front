import React, { Component } from 'react';
import { Row, Col, Card, CardHeader, CardBody } from 'reactstrap';
import { FaPhone } from 'react-icons/fa';
import './reportStyle.scss';

class TrueCallerReport extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Col sm={12} md={6} className="app-report">
                <Card className="truecaller-report">
                    <CardHeader>
                        <FaPhone /> TrueCaller
                    </CardHeader>
                    <CardBody>TrueCaller info</CardBody>
                </Card>
            </Col>
        );
    }
}

export default TrueCallerReport;
