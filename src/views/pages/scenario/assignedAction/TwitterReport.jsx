import React, { Component } from 'react';
import { Row, Col, Card, CardHeader, CardBody } from 'reactstrap';
import { FaTwitter } from 'react-icons/fa';
import './reportStyle.scss';

class TwitterReport extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Col sm={12} md={6} className="app-report">
                <Card className="twitter-report">
                    <CardHeader>
                        <FaTwitter /> Twitter
                    </CardHeader>
                    <CardBody>Twitter info</CardBody>
                </Card>
            </Col>
        );
    }
}

export default TwitterReport;
