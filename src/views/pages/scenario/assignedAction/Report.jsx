import React, { Component } from 'react';
import './reportStyle.scss';
import TelegramReport from './TelegramReport';
import InstagramReport from './InstagramReport';
import { connect } from 'react-redux';
import { getReportScenarioWorkflow } from '../../../../actions/scenarioActions';
import { withRouter } from 'react-router-dom';
import { Row } from 'reactstrap';
import FacebookReport from './FacebookReport';
import WhatsappReport from './WhatsappReport';
import TwitterReport from './TwitterReport';
import TrueCallerReport from './TrueCallerReport';

class Reports extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        let report_id = this.props.match.params.id;
        console.log(report_id)
        this.props.getReportScenarioWorkflow(report_id).then(() => {
            console.log(this.props.report_one)
        });
    }
    render() {
        // console.log(object)
        return (
            <div className="reports-container">
                <Row>
                    <TelegramReport />
                    <InstagramReport />
                    <FacebookReport />
                    <WhatsappReport />
                    <TwitterReport />
                    <TrueCallerReport />
                </Row>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    report_one: state.scenarioReducer.report_one
})

export default withRouter(connect(mapStateToProps, { getReportScenarioWorkflow })(Reports));
