import React, { Component } from 'react';
import {
    Form,
    Label,
    Input,
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    FormGroup,
} from 'reactstrap';
import { TiMediaPlay, TiMediaPlayOutline } from 'react-icons/ti';
import { runScenarioWorkflow } from '../../../../actions/scenarioActions';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';

class Play extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputs: {},
        };
    }
    onChange = e => {
        this.setState({
            inputs: {
                ...this.state.inputs,
                [e.target.name]: e.target.value,
            },
        });
    };
    onSubmit = e => {
        e.preventDefault();
        let data = {
            _id: this.props.scenarioId,
            workflows: this.props.workflow._id.$oid,
            inputs: this.state.inputs
        }
        this.props.runScenarioWorkflow(data).then(res => {
            if (res.status === "RUN IS OK") {
                toast(`Workflow has been ran successfully.`, { type: 'info' });
                this.props.close(e);
            } else {
                toast(res.error, { type: 'error' });
            }
        });
    };
    getInputType(inputType) {
        switch (inputType) {
            case 'tel':
                return 'tel';
            case 'username':
                return 'text';
            case 'email':
                return 'email';
            case 'lname':
                return 'text';
            case 'fname':
                return 'text';
        }
        return inputType;
    }
    getInputLabel(inputLabel) {
        switch (inputLabel) {
            case 'tel':
                return 'Phone Number';
            case 'username':
                return 'User Name';
            case 'email':
                return 'Email';
            case 'lname':
                return 'Last Name';
            case 'fname':
                return 'First Name';
        }
        return inputLabel;
    }
    getPlaceholder(inputPlaceholder) {
        switch (inputPlaceholder) {
            case 'tel':
                return 'enter phone number';
            case 'username':
                return 'enter username';
            case 'email':
                return 'enter email address';
            case 'lname':
                return 'enter last name';
            case 'fname':
                return 'enter first name';
        }
        return inputPlaceholder;
    }
    render() {
        // console.log(this.props.workflow);

        const magicForm = () => {
            let result =
                this.props.workflow.inputs &&
                this.props.workflow.inputs.map(item => {
                    return (
                        <FormGroup>
                            <Label for="scenario_name">{this.getInputLabel(item)}</Label>
                            <Input
                                type={this.getInputType(item)}
                                id="scenario_name"
                                name={item}
                                value={this.state.item}
                                onChange={this.onChange}
                                placeholder={this.getPlaceholder(item)}
                                required
                            />
                        </FormGroup>
                    );
                });
            return result;
        };

        return (
            <Modal isOpen={this.props.isOpen} toggle={e => this.props.close(e)}>
                <Form onSubmit={this.onSubmit}>
                    <ModalHeader toggle={e => this.props.close(e)}>
                        <h3>
                            <TiMediaPlay /> Start Workflow
                        </h3>
                    </ModalHeader>
                    <ModalBody>{magicForm()}</ModalBody>
                    <ModalFooter>
                        <Button color="warning" type="submit">
                            <TiMediaPlayOutline /> Start
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
        );
    }
}

export default connect(null, { runScenarioWorkflow })(Play);
