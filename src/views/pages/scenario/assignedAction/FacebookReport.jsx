import React, { Component } from 'react';
import { Row, Col, Card, CardHeader, CardBody } from 'reactstrap';
import { FaFacebook } from 'react-icons/fa';
import './reportStyle.scss';

class FacebookReport extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Col sm={12} md={6} className="app-report">
                <Card className="facebook-report">
                    <CardHeader>
                        <FaFacebook /> Facebook
                    </CardHeader>
                    <CardBody>Facebook info</CardBody>
                </Card>
            </Col>
        );
    }
}

export default FacebookReport;
