import React, { Component } from 'react';
import { Row, Col, Card, CardHeader, CardBody } from 'reactstrap';
import { FaTelegram, FaInstagram } from 'react-icons/fa';
import './reportStyle.scss';

class InstagramReport extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Col sm={12} md={6} className="app-report">
                <Card className="instagram-report">
                    <CardHeader>
                        <FaInstagram /> Instagram
                    </CardHeader>
                    <CardBody>Instagram info</CardBody>
                </Card>
            </Col>
        );
    }
}

export default InstagramReport;
