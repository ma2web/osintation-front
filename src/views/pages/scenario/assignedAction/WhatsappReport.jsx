import React, { Component } from 'react';
import { Row, Col, Card, CardHeader, CardBody } from 'reactstrap';
import { FaWhatsapp } from 'react-icons/fa';
import './reportStyle.scss';

class WhatsappReport extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Col sm={12} md={6} className="app-report">
                <Card className="whatsapp-report">
                    <CardHeader>
                        <FaWhatsapp /> Whatsapp
                    </CardHeader>
                    <CardBody>Whatsapp info</CardBody>
                </Card>
            </Col>
        );
    }
}

export default WhatsappReport;
