import React, { Component } from 'react';
import { Row, Col, Card, CardHeader, CardBody } from 'reactstrap';
import { FaTelegram } from 'react-icons/fa';
import './reportStyle.scss';

class TelegramReport extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Col sm={12} md={6} className="app-report">
                <Card className="telegram-report">
                    <CardHeader>
                        <FaTelegram /> Telegram
                    </CardHeader>
                    <CardBody>Telegram info</CardBody>
                </Card>
            </Col>
        );
    }
}

export default TelegramReport;
