import React, { Component } from 'react';
import {
    Form,
    Label,
    Input,
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    FormGroup,
} from 'reactstrap';
import { TiPlusOutline } from 'react-icons/ti';
import { connect } from 'react-redux';
import { createScenario, getAllScenarios } from '../../../actions/scenarioActions';
import { toast } from 'react-toastify';

export class CreateScenario extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scenario_name: '',
            description: '',
        };
    }
    onChange = e => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };
    onSubmit = e => {
        e.preventDefault();
        this.props.createScenario(this.state).then(res => {
            if (res.status === 'OK') {
                this.props.getAllScenarios().then(() => {
                    toast(`Scenario has been added successfully.`, { type: 'info' });
                    this.props.close(e);
                    this.setState({
                        scenario_name: '',
                        description: '',
                    })
                });
            } else {
                toast(res.error, { type: 'error' });
            }
        });
    };
    render() {
        return (
            <Modal isOpen={this.props.isOpen} toggle={e => this.props.close(e)}>
                <Form onSubmit={this.onSubmit}>
                    <ModalHeader toggle={e => this.props.close(e)}>
                        <h3>Add Scenario</h3>
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="scenario_name">Name</Label>
                            <Input
                                type="text"
                                id="scenario_name"
                                name="scenario_name"
                                value={this.state.scenario_name}
                                onChange={this.onChange}
                                required
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="description">Description</Label>
                            <Input
                                type="textarea"
                                name="description"
                                id="description"
                                value={this.state.description}
                                onChange={this.onChange}
                            />
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" type="submit">
                            <TiPlusOutline /> add
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
        );
    }
}

export default connect(null, { createScenario, getAllScenarios })(CreateScenario);
