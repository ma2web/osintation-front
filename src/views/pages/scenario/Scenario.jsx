import React, { Component } from 'react';
import { Row, Col, Button } from 'reactstrap';
import { connect } from 'react-redux';
import { getOneScenario, updateScenario, addWorkflow } from '../../../actions/scenarioActions';
import { Loader } from '../../../vibe';
import { TiEdit, TiPlusOutline } from 'react-icons/ti';
import { UpdateScenario } from './UpdateScenario';
import AssignedWorkflows from './AssignedWorkflows';
import AssignWorkflow from './AssignWorkflow';

export class Scenario extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            isEditModalOpen: false,
            isOpenAssignModal: false,
        };
    }
    componentDidMount() {
        const { id } = this.props.match.params;
        this.props.getOneScenario(id).then(() => {
            this.setState({
                loading: false,
            });
        });
    }
    openEditModal = e => {
        this.setState({
            isEditModalOpen: !this.state.isEditModalOpen,
        });
    };
    closeEditModal = e => {
        e.preventDefault();
        this.setState({
            isEditModalOpen: false,
        });
    };
    openAssignModal = e => {
        e.preventDefault();
        this.setState({
            isOpenAssignModal: true,
        });
    };
    closeAssignModal = e => {
        e.preventDefault();
        this.setState({
            isOpenAssignModal: false,
        });
    };
    render() {
        return (
            <div className="scenarios">
                <UpdateScenario
                    isOpen={this.state.isEditModalOpen}
                    close={this.closeEditModal}
                    scenarioId={this.props.scenario._id && this.props.scenario._id.$oid}
                    scenarioName={this.props.scenario.scenario_name}
                    scenarioDescription={this.props.scenario.description}
                    getOneScenario={this.props.getOneScenario}
                    updateScenario={this.props.updateScenario}
                />
                <AssignWorkflow
                    isOpen={this.state.isOpenAssignModal}
                    close={this.closeAssignModal}
                    scenarioId={this.props.scenario._id && this.props.scenario._id.$oid}
                    getAllWorkflows={this.props.getAllWorkflows}
                    addWorkflow={this.props.addWorkflow}
                />
                {!this.state.loading ? (
                    <Row>
                        <Col className="col-sm-12">
                            <div className="scenario-info mb-3">
                                <div className="scenario-name">
                                    <h1>{this.props.scenario.scenario_name}</h1>
                                </div>
                                <div className="scenario-description">
                                    <p>{this.props.scenario.description}</p>
                                </div>
                                <div className="scenario-workflow-counts">
                                    <p>
                                        {this.props.scenario.workflow &&
                                        this.props.scenario.workflow.length > 0
                                            ? 'Workflow(s): ' + this.props.scenario.workflow.length
                                            : 'No Workflow!'}
                                    </p>
                                </div>
                                <div className="scenario-actions text-right">
                                    <Button
                                        color="warning"
                                        size="sm"
                                        onClick={this.openEditModal}
                                        outline
                                    >
                                        <TiEdit size="1.5em" />
                                    </Button>
                                </div>
                            </div>
                        </Col>
                        <Col className="col-sm-12 text-right mb-3">
                            <Button color="info" size="sm" onClick={this.openCreateModal}>
                                <TiPlusOutline /> Report
                            </Button>{' '}
                            <Button color="warning" size="sm" onClick={this.openAssignModal}>
                                <TiPlusOutline /> Workflow
                            </Button>
                        </Col>

                        <AssignedWorkflows
                            scenario={this.props.scenario}
                            scenarioId={this.props.scenario._id && this.props.scenario._id.$oid}
                            getOneScenario={this.props.getOneScenario}
                        />
                    </Row>
                ) : (
                    <Row>
                        <Col className="loader-container">
                            <Loader type="puff" />
                        </Col>
                    </Row>
                )}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    scenario: state.scenarioReducer.scenario,
});

export default connect(mapStateToProps, { getOneScenario, updateScenario, addWorkflow })(Scenario);
