import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getAllScenarios } from '../../../actions/scenarioActions';
import { Row, Col, Button, Card, CardTitle, CardText } from 'reactstrap';
import { Loader } from '../../../vibe';
import { TiPlusOutline, TiTrash, TiChevronRightOutline, TiInfoLarge } from 'react-icons/ti';
import CreateScenario from './CreateScenario';
import DeleteScenario from './DeleteScenario';

class SenarioLists extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            createModal: false,
            deletModal: false,
            _id: '',
        };
    }
    componentDidMount() {
        this.props.getAllScenarios().then(() => {
            this.setState({
                loading: false,
            });
        });
    }
    openCreateModal = () => {
        this.setState({
            createModal: true,
        });
    };
    closeCreateModal = e => {
        e.preventDefault();
        this.setState({
            createModal: false,
        });
    };
    openDeleteModal = id => {
        this.setState({
            deletModal: true,
            _id: id,
        });
    };
    closeDeleteModal = e => {
        e.preventDefault();
        this.setState({
            deletModal: false,
        });
    };
    render() {
        return (
            <div className="scenarios">
                <CreateScenario 
                    isOpen={this.state.createModal} 
                    close={this.closeCreateModal} 
                />
                <DeleteScenario
                    isOpen={this.state.deletModal}
                    close={this.closeDeleteModal}
                    id={this.state._id}
                />
                <div className="create-scenario text-right mb-3">
                    <Button color="warning" onClick={this.openCreateModal}>
                        <TiPlusOutline /> Add Scenario
                    </Button>
                </div>
                <Row>
                    {!this.state.loading ? (
                        Array.isArray(this.props.scenarios) && this.props.scenarios.length ? (
                            this.props.scenarios &&
                            this.props.scenarios.map(item => {
                                return (
                                    <Col className="col-sm-12 col-md-6 col-xl-4">
                                        <Card key={item._id.$oid} className="scenario-card" body>
                                            <CardTitle>
                                                <TiChevronRightOutline /> {item.scenario_name}
                                            </CardTitle>
                                            <CardText>{item.description}</CardText>
                                            <Row>
                                                <Col className="text-right">
                                                    <Link to={`/scenario/${item._id.$oid}`}>
                                                        <Button size="sm" color="info">
                                                            <TiInfoLarge size="1.5em" />
                                                        </Button>{' '}
                                                    </Link>
                                                    <Button
                                                        size="sm"
                                                        color="danger"
                                                        onClick={e =>
                                                            this.openDeleteModal(item._id.$oid)
                                                        }
                                                    >
                                                        <TiTrash size="1.5em" />
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </Card>
                                    </Col>
                                );
                            })
                        ) : (
                            <Col className="nothing-have-to-show">
                                <p>Have nothing to show!</p>
                            </Col>
                        )
                    ) : (
                        <Col className="loader-container">
                            <Loader type="puff" />
                        </Col>
                    )}
                </Row>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    scenarios: state.scenarioReducer.scenarios,
});

export default connect(mapStateToProps, { getAllScenarios })(SenarioLists);
