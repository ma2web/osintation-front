import React, { Component } from 'react';
import { Button, Col } from 'reactstrap';
import { TiTrash, TiEye, TiMediaPlayOutline, TiMediaStopOutline } from 'react-icons/ti';
import { Loader } from '../../../vibe';
import {
    removeWorkflow,
    pauseScenarioWorkflow,
    reportScenarioWorkflow,
} from '../../../actions/scenarioActions';
import { getOneWorkflow } from '../../../actions/workflowActions';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import Play from './assignedAction/Play';
import Reports from './assignedAction/Report';
import { Redirect, withRouter } from 'react-router-dom';

class AssignedWorkflows extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            playModal: false,
            reportModal: false,
            redirect: false
        };
    }
    delAssignedWorkflow = wid => {
        console.log('scenario id', this.props.scenario._id.$oid);
        console.log('workflow id', wid);
        let workflow = {
            _id: this.props.scenario._id.$oid,
            workflow: wid,
        };
        this.props.removeWorkflow(workflow).then(res => {
            if (res.status === 'OK') {
                this.props.getOneScenario(this.props.scenarioId).then(() => {
                    toast(`Workflow has been deassigned successfully.`, { type: 'info' });
                });
            } else {
                toast(res.error, { type: 'error' });
            }
        });
    };
    playModal(id) {
        this.props.getOneWorkflow(id).then(() => {
            this.setState({
                playModal: true,
            });
        });
    }
    closePlayModal = e => {
        e.preventDefault();
        this.setState({
            playModal: false,
        });
    };
    handlePaused = wId => {
        let data = {
            _id: this.props.scenario._id.$oid,
            workflows: wId,
        };
        this.props.pauseScenarioWorkflow(data).then(res => {
            if (res.status === 'PAUSE IS OK') {
                toast(`Workflow has been stopped successfully.`, { type: 'info' });
            } else {
                toast(res.error, { type: 'error' });
            }
        });
    };
    reportModal = (wid) => {
        let data = {
            _id: this.props.scenario._id.$oid,
            workflows: wid
        }
        this.props.reportScenarioWorkflow(data).then((res) => {
            if(res.report) {
                this.setState({
                    redirect: true
                })
            }
            console.log(this.props.report.report_id)
        })
    };
    closeReportModal = e => {
        e.preventDefault();
        this.setState({
            reportModal: false,
        });
    };
    render() {
        if(this.state.redirect) {
            return <Redirect to={`/report/${this.props.report.report_id}`} />
        }
        return (
            <React.Fragment>
                <Play
                    isOpen={this.state.playModal}
                    close={this.closePlayModal}
                    workflow={this.props.workflow}
                    scenarioId={this.props.scenario._id.$oid}
                />
                {!this.state.loading ? (
                    Array.isArray(this.props.scenario.workflow) &&
                    this.props.scenario.workflow.length ? (
                        <Col className="col-sm-12">
                            <table className="osint-table scenario-table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>App(s)</th>
                                        <th>Created at</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.scenario.workflow.map(item => {
                                        const date = new Date(item.created_at.$date);
                                        return (
                                            <tr key={item._id.$oid}>
                                                <th scope="row">{item.workflow_name}</th>
                                                <th scope="row">{item.extractors.join(', ')}</th>
                                                <td>{date.toGMTString()}</td>
                                                <td className="action-btns">
                                                    <Button
                                                        size="sm"
                                                        color="warning"
                                                        onClick={() =>
                                                            this.playModal(item._id.$oid)
                                                        }
                                                    >
                                                        <TiMediaPlayOutline size="1.5em" />
                                                    </Button>{' '}
                                                    <Button
                                                        size="sm"
                                                        color="danger"
                                                        onClick={() =>
                                                            this.handlePaused(item._id.$oid)
                                                        }
                                                    >
                                                        <TiMediaStopOutline size="1.5em" />
                                                    </Button>{' '}
                                                    <Button
                                                        size="sm"
                                                        color="success"
                                                        onClick={() => this.reportModal(item._id.$oid)}
                                                    >
                                                        <TiEye size="1.5em" />
                                                    </Button>{' '}
                                                    <Button
                                                        size="sm"
                                                        color="danger"
                                                        onClick={() =>
                                                            this.delAssignedWorkflow(item._id.$oid)
                                                        }
                                                    >
                                                        <TiTrash size="1.5em" />
                                                    </Button>
                                                </td>
                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </table>
                        </Col>
                    ) : (
                        <Col className="nothing-have-to-show">
                            <p>No workflow!</p>
                        </Col>
                    )
                ) : (
                    <Col className="loader-container">
                        <Loader type="puff" />
                    </Col>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    workflow: state.workflowReducer.workflow,
    report: state.scenarioReducer.report,
});

export default withRouter(connect(mapStateToProps, {
    removeWorkflow,
    getOneWorkflow,
    pauseScenarioWorkflow,
    reportScenarioWorkflow,
})(AssignedWorkflows));
