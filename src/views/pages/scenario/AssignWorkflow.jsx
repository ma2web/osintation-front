import React, { Component } from 'react';
import { Form, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { TiCancel, TiPlusOutline } from 'react-icons/ti';
import { connect } from 'react-redux';
import { getAllWorkflows } from '../../../actions/workflowActions';
import { getOneScenario } from '../../../actions/scenarioActions';
import { toast } from 'react-toastify';

export class AssignWorkflows extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            assignWorkflow: {
                _id: '',
                workflows: '',
            },
        };
    }
    componentDidMount() {
        this.props.getAllWorkflows().then(() => {
            this.setState({
                loading: false,
            });
        });
    }
    assignWorkflow = (e, id) => {
        e.preventDefault();
        console.log(id);
        console.log(this.props.scenarioId);
        const data = {
            _id: this.props.scenarioId,
            workflow: id,
        };
        console.log(data);
        this.props.addWorkflow(data).then(res => {
            if (res.status === 'OK') {
                console.log(this.props.getOneScenario(this.props.scenarioId));
                this.props.getOneScenario(this.props.scenarioId).then(() => {
                    toast(`Workflow has been added successfully to scenario.`, { type: 'info' });
                    this.props.close(e);
                });
            } else {
                toast(res.error, { type: 'error' });
            }
        });
    };
    render() {
        return (
            <Modal isOpen={this.props.isOpen} toggle={e => this.props.close(e)}>
                <ModalHeader toggle={e => this.props.close(e)}>
                    <h3>Assign Workflow</h3>
                </ModalHeader>
                <ModalBody>
                    {!this.state.loading ? (
                        Array.isArray(this.props.workflows) && this.props.workflows.length ? (
                            <table className="osint-table modal-table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Apps</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.workflows.length &&
                                        this.props.workflows.map(item => {
                                            return (
                                                <tr key={item._id.$oid}>
                                                    <td>{item.workflow_name}</td>
                                                    <td>{item.extractors.join(', ')}</td>
                                                    <td>
                                                        <Button
                                                            color="warning"
                                                            type="submit"
                                                            size="sm"
                                                            outline
                                                            onClick={e => {
                                                                this.assignWorkflow(
                                                                    e,
                                                                    item._id.$oid
                                                                );
                                                            }}
                                                        >
                                                            <TiPlusOutline />
                                                        </Button>
                                                    </td>
                                                </tr>
                                            );
                                        })}
                                </tbody>
                            </table>
                        ) : (
                            <div>No dta</div>
                        )
                    ) : (
                        <div>Loading....</div>
                    )}
                </ModalBody>
                <ModalFooter>
                    <Button color="danger" type="submit" onClick={e => this.props.close(e)}>
                        <TiCancel /> close
                    </Button>
                </ModalFooter>
            </Modal>
        );
    }
}

const mapStateToProps = state => ({
    workflows: state.workflowReducer.workflows,
});

export default connect(mapStateToProps, { getAllWorkflows, getOneScenario })(AssignWorkflows);
