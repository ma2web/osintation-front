import React, { Component } from 'react';
import { connect } from 'react-redux';
import { login } from '../../../actions/auth';
import { Redirect } from 'react-router-dom';
import {
    Row,
    Col,
    Button,
    Form,
    FormGroup,
    Label,
    Input,
    Card,
    CardHeader,
    CardFooter,
    CardBody,
    UncontrolledAlert,
} from 'reactstrap';
import SocialBG from '../../../assets/images/loginBg.jpg';
import RasaLab from '../../../assets/images/rasaLab.png';
import { TiPower } from 'react-icons/ti';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                email: '',
                password: '',
            },
            showAlert: false,
            redirect: false,
        };
    }
    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };
    handleSubmit = e => {
        e.preventDefault();
        const { email, password } = this.state;
        this.props.login(email, password)
    };
    render() {
        if (this.props.me) {
            return <Redirect to="/home" />;
        }
        return (
            <div className="login-page">
                <Row className="login-form-row">
                    <Col md="4"></Col>
                    <Col className="col-sm-12 col-md-6 col-xl-4">
                        {
                            <UncontrolledAlert isOpen={this.state.showAlert} color="danger">
                                {`status: ${this.props.error.status} message: ${this.props.error.message} `}
                            </UncontrolledAlert>
                        }
                        <Card>
                            <Form onSubmit={this.handleSubmit}>
                                <CardHeader tag="h3" className="login-header-form">
                                    Login Form
                                </CardHeader>
                                <CardBody>
                                    <FormGroup>
                                        <Label for="username">Email</Label>
                                        <Input
                                            type="email"
                                            name="email"
                                            id="email"
                                            placeholder="Enter email"
                                            value={this.state.email}
                                            onChange={this.handleChange}
                                            required
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="password">Password</Label>
                                        <Input
                                            type="password"
                                            name="password"
                                            placeholder="Enter password"
                                            id="password"
                                            value={this.state.password}
                                            onChange={this.handleChange}
                                            required
                                        />
                                    </FormGroup>
                                </CardBody>
                                <CardFooter className="text-muted text-right">
                                    <Button color="primary" right>
                                        <TiPower /> Login
                                    </Button>
                                </CardFooter>
                            </Form>
                        </Card>
                    </Col>
                    <Col md="4"></Col>
                </Row>
                <div className="login-bg">
                    <img src={SocialBG} alt="bg" />
                </div>
                <div className="rasa-lab">
                    <img src={RasaLab} alt="rasa lab" />
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    me: state.auth.me,
    error: state.auth,
});

export default connect(mapStateToProps, { login })(Login);
