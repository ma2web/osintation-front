/* eslint-disable jsx-a11y/img-redundant-alt */
import React from 'react';
import { FaTelegram, FaInfo, FaCheck, FaRegTimesCircle, FaSearchengin, FaListAlt, FaRecycle } from "react-icons/fa";
import {
  Button,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardHeader,
  CardText,
  CardFooter
} from 'reactstrap';

function Telegram({...props}) {
    return(
        
            <Col md={12}>
                <Card>
                    <CardHeader className="socialHeaderContainer">
                        <CardTitle>
                            <FaTelegram size="2rem" color="white" className="socialIcon" />
                            <span className="socialName">Telegram</span>
                        </CardTitle>
                    </CardHeader>
                    <CardBody className="socialBodyContainer">
                        <CardText>
                            {
                                ( props.status === "No info" ) ?
                                
                                    <div className="noInfoWrapper">
                                        <p className="statusParagraph" style={{color:"rgb(15, 56, 47)"}}>
                                            <FaInfo />Press check button
                                        </p>
                                    </div> :

                                ( props.status === "Installed" ) ?

                                    <div className="installedWrapper">
                                        <p className="statusParagraph" style={{color:"rgb(34,182,110)"}}>
                                            <FaCheck />{props.status}
                                        </p>
                                        <div className="profileInfo">
                                            <div className="profileImage">
                                                <img src={props.profileimage} alt="profile image" ></img>
                                            </div>
                                            <div className="profileSummary">
                                                <p className="profileInfoItem">
                                                    Username: <span>{props.username}</span>
                                                </p>
                                                <p className="profileInfoItem">
                                                    Bio: <span>{props.bio}</span>
                                                </p>
                                                <p className="profileInfoItem">
                                                    Last seen: <span>{props.lastseen} days ago</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div> :

                                <div className="notInstalledWrapper">
                                    <p className="statusParagraph" style={{color:"rgb(231,76,75)"}}>
                                        <FaRegTimesCircle />{props.status}
                                    </p>
                                    <p style={{fontSize:13}}>
                                        this app is not installed on current number
                                    </p>
                                </div>
                            }
                        </CardText>
                    </CardBody>
                    <CardFooter className="socialFooterContainer text-right">
                        
                        {
                            ( props.status === "No info" ) ?
                                <Button color="primary" size="sm" onClick={() => props.telegramHandleClick()}><FaSearchengin /> Check Number</Button>
                            :
                            ( props.status === "Installed" ) ?
                                <Button color="info" size="sm"><FaListAlt /> More info</Button>
                            :
                                <Button color="warning" size="sm"><FaRecycle /> ReCheck</Button>
                        }

                    </CardFooter>
                </Card>
            </Col>
    )
}

export default Telegram