import React from 'react';
import Telegram from './Telegram'
import Instagram from './Instagram'
import Facebook from './Facebook'
import Whatsapp from './Whatsapp'
import Twitter from './Twitter'
import { FaStar } from "react-icons/fa";
import {
    Button,
    Row,
    Col,
    Form,
    Input
} from 'reactstrap';

class SocialChecker extends React.Component {
    state = {
        socials: {
            telegram: {
                result: {
                    profileimage: "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/1024px-Circle-icons-profile.svg.png",
                    statusId: 1,
                    // "No info", "Installed", "Not Installed"
                    status: "No info",
                    lastseen: new Date().getDay(),
                    bio: "Some text here",
                    username: "@telegram_username",
                    loading: false,
                }
            },
            facebook: {
                result: {
                    profileimage: "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/1024px-Circle-icons-profile.svg.png",
                    status: "No info",
                    lastseen: new Date().getDay(),
                    bio: "Some text here",
                    username: "@facebook_username",
                    posts: "14",
                    friends: "530"
                }
            },
            whatsapp: {
                result: {
                    profileimage: "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/1024px-Circle-icons-profile.svg.png",
                    status: "No info",
                    lastseen: new Date().getDay(),
                    bio: "Some text here",
                    username: "@whatsapp_username",
                    telegramHandleClick: false
                }
            },
            instagram: {
                result: {
                    profileimage: "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/1024px-Circle-icons-profile.svg.png",
                    status: "No info",
                    lastseen: new Date().getDay(),
                    bio: "Some text here",
                    username: "@instagram_username",
                    follower: "140",
                    following: "230"
                }
            },
            twitter: {
                result: {
                    profileimage: "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/1024px-Circle-icons-profile.svg.png",
                    status: "No info",
                    posts: 122,
                    bio: "Some text here",
                    username: "@instagram_username",
                    tags: ["html", " ,css", " ,js"],
                }
            }
        }
    }

    telegramHandleClick = () => {
        // alert('telegram checked')
        const socials = this.state.socials
        socials.telegram.result.status = "Installed"
        this.setState({
            socials
        })
    }

    facebookHandleClick = () => {
        // alert('facebook checked')
        const socials = this.state.socials
        socials.facebook.result.status = "Not Installed"
        this.setState({
            socials
        })
    }

    whatsappHandleClick = () => {
        // alert('whatsapp checked')
        const socials = this.state.socials
        socials.whatsapp.result.status = "Installed"
        this.setState({
            socials
        })
    }

    instagramHandleClick = () => {
        // alert('instagram checked')
        const socials = this.state.socials
        socials.instagram.result.status = "Installed"
        this.setState({
            socials
        })
    }

    twitterHandleClick = () => {
        // alert('instagram checked')
        const socials = this.state.socials
        socials.twitter.result.status = "Installed"
        this.setState({
            socials
        })
    }

    checkAll = () => {
        this.telegramHandleClick()
        this.facebookHandleClick()
        this.whatsappHandleClick()
        this.instagramHandleClick()
        this.twitterHandleClick()
    }

    render() {
        const {lastseen, status, profileimage, bio, username, loading} = this.state.socials.telegram.result
        return (
            <React.Fragment>
                <Row sm={12} className="rowContainer">
                    <Col md={12}>
                        <Form inline>
                            <Input type="text" name="target" id="number" placeholder="number / username / phone number ..." />
                            {'\u00A0'}
                            <Button outline color="warning"><FaStar style={{marginTop: "-5px"}} /></Button>
                            {'\u00A0 \u00A0'}
                            <Button color="warning" onClick={this.checkAll}>Check All</Button>
                        </Form>
                    </Col>
                </Row>
                <hr />
                <br />
                <Row sm={12} className="rowSocialContainer">
                    <Col md={4}>
                        <Telegram 
                            status={status}
                            lastseen={lastseen}
                            profileimage={profileimage}
                            bio={bio}
                            username={username}
                            telegramHandleClick={this.telegramHandleClick}
                            loading={loading} />
                    </Col>
                    <Col md={4}>
                        <Facebook 
                            status={this.state.socials.facebook.result.status}
                            lastseen={this.state.socials.facebook.result.lastseen}
                            profileimage={this.state.socials.facebook.result.profileimage}
                            bio={this.state.socials.facebook.result.bio}
                            username={this.state.socials.facebook.result.username}
                            posts={this.state.socials.facebook.result.posts}
                            friends={this.state.socials.facebook.result.friends}
                            facebookHandleClick={this.facebookHandleClick} />
                    </Col>
                    <Col md={4}>
                        <Whatsapp 
                            status={this.state.socials.whatsapp.result.status}
                            lastseen={this.state.socials.whatsapp.result.lastseen}
                            profileimage={this.state.socials.whatsapp.result.profileimage}
                            bio={this.state.socials.whatsapp.result.bio}
                            username={this.state.socials.whatsapp.result.username}
                            follower={this.state.socials.whatsapp.result.follower}
                            following={this.state.socials.whatsapp.result.following}
                            whatsappHandleClick={this.whatsappHandleClick} />
                    </Col>
                    <Col md={4}>
                        <Instagram 
                            status={this.state.socials.instagram.result.status}
                            lastseen={this.state.socials.instagram.result.lastseen}
                            profileimage={this.state.socials.instagram.result.profileimage}
                            bio={this.state.socials.instagram.result.bio}
                            username={this.state.socials.instagram.result.username}
                            follower={this.state.socials.instagram.result.follower}
                            following={this.state.socials.instagram.result.following}
                            instagramHandleClick={this.instagramHandleClick} />
                    </Col>
                    <Col md={4}>
                        <Twitter 
                            status={this.state.socials.twitter.result.status}
                            posts={this.state.socials.twitter.result.posts}
                            profileimage={this.state.socials.twitter.result.profileimage}
                            bio={this.state.socials.twitter.result.bio}
                            username={this.state.socials.twitter.result.username}
                            tags={this.state.socials.twitter.result.tags}
                            twitterHandleClick={this.twitterHandleClick} />
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}
 
export default SocialChecker;