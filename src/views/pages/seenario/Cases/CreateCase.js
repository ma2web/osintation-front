import React, { Component } from 'react';
import { FaRegPlusSquare } from 'react-icons/fa';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { createCase } from '../../../../actions/caseAction';
import {
    Form,
    Label,
    Input,
    Button,
    Col,
    Row,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
} from 'reactstrap';

class CreateCase extends Component {
    constructor(props) {
        super(props);
        this.state = {
            caseName: '',
            caseType: '',
            modal: false,
        };
    }
    onChange = e => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };
    onSubmit = e => {
        e.preventDefault();
        const _case = {
            caseName: this.state.caseName,
            caseType: this.state.caseType,
        };
        this.props.createCase(_case);
        this.setState({
            modal: false,
        });
    };
    toggleModal = () => {
        this.setState(prevState => ({
            modal: !prevState.modal,
        }));
    };
    render() {
        return (
            <Row>
                <Col>
                    <div className="casesHeader text-right">
                        <Button color="warning" onClick={this.toggleModal}>
                            <FaRegPlusSquare /> Add Case
                        </Button>
                    </div>
                    <Modal isOpen={this.state.modal} toggle={this.toggleModal}>
                        <Form onSubmit={this.onSubmit}>
                            <ModalHeader toggle={this.toggleModal}>
                                <h3>Add Case</h3>
                            </ModalHeader>
                            <ModalBody>
                                <Label>Case Name</Label>
                                <Input
                                    type="text"
                                    name="caseName"
                                    value={this.state.caseName}
                                    onChange={this.onChange}
                                    required
                                />
                                <br />
                                <Label>Case Type</Label>
                                <Input
                                    type="select"
                                    name="caseType"
                                    value={this.state.caseType}
                                    onChange={this.onChange}
                                    required
                                >
                                    <option>---select---</option>
                                    <option>Basic</option>
                                    <option>Advance</option>
                                </Input>
                                <br />
                            </ModalBody>
                            <ModalFooter>
                                <Button color="primary" type="submit">
                                    Create Case
                                </Button>{' '}
                                <Button color="warning" onClick={this.toggleModal}>
                                    Cancel
                                </Button>
                            </ModalFooter>
                        </Form>
                    </Modal>
                </Col>
            </Row>
        );
    }
}

createCase.propTypes = {
    createCase: PropTypes.func.isRequired,
};

export default connect(null, { createCase })(CreateCase);
