import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllWorkflows } from '../../../../actions/workflowActions';
import { Row, Col, Button } from 'reactstrap';
import CreateCase from './CreateCase';
import { Loader } from '../../../../vibe';

class Cases extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
        };
    }
    componentDidMount() {
        this.props.getAllWorkflows().then(() => {
            this.setState({
                loading: false,
            });
        });
    }
    render() {
        return (
            <Row>
                <Col>
                    <main>
                        <CreateCase />
                        <div className="caseContainer">
                            {!this.state.loading ? (
                                Array.isArray(this.props.cases) && this.props.cases.length ? (
                                    this.props.cases &&
                                    this.props.cases.map(user => {
                                        return (
                                            
                                            <div key={user.id} className="caseWrapper">
                                                <div className="caseInfo">
                                                    <h4>{user.name}</h4>
                                                    <p className="caseName">
                                                        <span>Target:</span> {user.name}
                                                    </p>
                                                    <p className="caseUserNAme">
                                                        <span>Case Number:</span> {user.id}
                                                    </p>
                                                    <p>
                                                        <span>Case Type:</span> {user.username}
                                                    </p>
                                                </div>
                                                <br />
                                                <div>
                                                    {user.username === 'Advance' ? (
                                                        <Button outline color="warning" size="sm">
                                                            Scenario
                                                        </Button>
                                                    ) : (
                                                        ''
                                                    )}
                                                </div>
                                            </div>
                                        );
                                    })
                                ) : (
                                    <p>Nothing</p>
                                )
                            ) : (
                                <Loader type="spin" />
                            )}
                        </div>
                    </main>
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    workflows: state.workflowReducer.workflows,
});

export default connect(mapStateToProps, { getAllWorkflows })(Cases);
