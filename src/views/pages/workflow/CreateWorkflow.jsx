import React, { Component } from 'react';
import {
    Form,
    Label,
    Input,
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    FormGroup,
    ListGroup,
    ListGroupItem,
    Row,
    Col,
} from 'reactstrap';
import { TiPlusOutline } from 'react-icons/ti';
import { connect } from 'react-redux';
import { createWorkflow, getAllWorkflows } from '../../../actions/workflowActions';
import { toast } from 'react-toastify';
import { FaTelegram } from 'react-icons/fa';

export class CreateScenario extends Component {
    constructor(props) {
        super(props);
        this.state = {
            workflow_name: '',
            description: '',
            extractors: [],
            inputs: [],
        };
    }
    selectApp = e => {
        let joined = this.state.extractors.concat(e.target.value);
        this.setState({
            extractors: joined,
        });
    };
    selectInputs = e => {
        let joined = this.state.inputs.concat(e.target.value);
        this.setState({
            inputs: joined,
        });
        console.log(this.state.inputs);
    };
    onChange = e => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };
    onSubmit = e => {
        e.preventDefault();
        console.log(this.state);
        this.props.createWorkflow(this.state).then(res => {
            if (res.status === 'OK') {
                this.props.getAllWorkflows().then(() => {
                    toast(`Workflow has been added successfully.`, { type: 'info' });
                    this.props.close(e);
                    this.setState({
                        workflow_name: '',
                        description: '',
                    });
                });
            } else {
                toast(res.error, { type: 'error' });
            }
        });
    };
    render() {
        return (
            <Modal isOpen={this.props.isOpen} toggle={e => this.props.close(e)}>
                <Form onSubmit={this.onSubmit}>
                    <ModalHeader toggle={e => this.props.close(e)}>
                        <h3>Add Workflow</h3>
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="workflow_name">Name</Label>
                            <Input
                                type="text"
                                id="workflow_name"
                                name="workflow_name"
                                value={this.state.scenario_name}
                                onChange={this.onChange}
                                required
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="description">Description</Label>
                            <Input
                                type="textarea"
                                name="description"
                                id="description"
                                value={this.state.description}
                                onChange={this.onChange}
                            />
                        </FormGroup>
                        {/* <FormGroup>
                            <Label for="extractors">Apps</Label>
                            <Input
                                type="select"
                                name="extractors"
                                id="extractors"
                                multiple
                                value={this.state.extractors}
                                onChange={this.selectApp}
                            >
                                <option value="Telegram">Telegram</option>
                                <option value="Whatsapp">Whatsapp</option>
                                <option value="Truecaller">Truecaller</option>
                                <option value="Facebook">Facebook</option>
                                <option value="Twitter">Twitter</option>
                            </Input>
                        </FormGroup> */}
                        <Row>
                            <Col className="col-sm-12 col-md-6">
                                <FormGroup className="extractors-checkbox">
                                    <Label for="extractors">Apps</Label>
                                    <ListGroup>
                                        <ListGroupItem color="success">
                                            <Input
                                                id="Whatsapp"
                                                type="checkbox"
                                                value="Whatsapp"
                                                onChange={this.selectApp}
                                            />{' '}
                                            <Label for="Whatsapp">Whatsapp</Label>
                                        </ListGroupItem>
                                        <ListGroupItem color="info">
                                            <Input
                                                id="Telegram"
                                                type="checkbox"
                                                value="Telegram"
                                                onChange={this.selectApp}
                                            />{' '}
                                            <Label for="Telegram">Telegram</Label>
                                        </ListGroupItem>
                                        <ListGroupItem color="warning">
                                            <Input
                                                id="Facebook"
                                                type="checkbox"
                                                value="Facebook"
                                                onChange={this.selectApp}
                                            />{' '}
                                            <Label for="Facebook">Facebook</Label>
                                        </ListGroupItem>
                                        <ListGroupItem color="danger">
                                            <Input
                                                id="Truecaller"
                                                type="checkbox"
                                                value="Truecaller"
                                                onChange={this.selectApp}
                                            />{' '}
                                            <Label for="Truecaller">Truecaller</Label>
                                        </ListGroupItem>
                                        <ListGroupItem color="info">
                                            <Input
                                                id="Twitter"
                                                type="checkbox"
                                                value="Twitter"
                                                onChange={this.selectApp}
                                            />{' '}
                                            <Label for="Twitter">Twitter</Label>
                                        </ListGroupItem>
                                    </ListGroup>
                                </FormGroup>
                            </Col>
                            <Col className="col-sm-12 col-md-6">
                                <FormGroup className="extractors-checkbox">
                                    <Label for="extractors">Form Inputs</Label>
                                    <ListGroup>
                                        <ListGroupItem color="success">
                                            <Input
                                                id="tel"
                                                type="checkbox"
                                                value="tel"
                                                onChange={this.selectInputs}
                                            />{' '}
                                            <Label for="tel">Tel</Label>
                                        </ListGroupItem>
                                        <ListGroupItem color="info">
                                            <Input
                                                id="username"
                                                type="checkbox"
                                                value="username"
                                                onChange={this.selectInputs}
                                            />{' '}
                                            <Label for="username">Username</Label>
                                        </ListGroupItem>
                                        <ListGroupItem color="warning">
                                            <Input
                                                id="email"
                                                type="checkbox"
                                                value="email"
                                                onChange={this.selectInputs}
                                            />{' '}
                                            <Label for="email">Email</Label>
                                        </ListGroupItem>
                                        <ListGroupItem color="danger">
                                            <Input
                                                id="fname"
                                                type="checkbox"
                                                value="fname"
                                                onChange={this.selectInputs}
                                            />{' '}
                                            <Label for="fname">First Name</Label>
                                        </ListGroupItem>
                                        <ListGroupItem color="info">
                                            <Input
                                                id="lname"
                                                type="checkbox"
                                                value="lname"
                                                onChange={this.selectInputs}
                                            />{' '}
                                            <Label for="lname">Last Name</Label>
                                        </ListGroupItem>
                                    </ListGroup>
                                </FormGroup>
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" type="submit">
                            <TiPlusOutline /> add
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
        );
    }
}

export default connect(null, { createWorkflow, getAllWorkflows })(CreateScenario);
