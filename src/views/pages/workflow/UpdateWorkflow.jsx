import React, { Component } from 'react';
import {
    Form,
    Label,
    Input,
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    FormGroup,
} from 'reactstrap';
import { TiArrowRepeat } from 'react-icons/ti';
import { toast } from 'react-toastify';
import { updateWorkflow, getAllWorkflows } from '../../../actions/workflowActions';
import { connect } from 'react-redux';

export class UpdateWorkflow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            _id: '',
            workflow: {
                workflow_name: '',
                description: '',
                extractors: [],
            },
        };
    }
    onChange = e => {
        this.setState({
            workflow: {
                ...this.state.workflow,
                [e.target.name]: e.target.value,
            },
        });
    };
    onSubmit = e => {
        e.preventDefault();
        this.props.updateWorkflow(this.state).then(res => {
            if (res.status === 'OK') {
                this.props.getAllWorkflows().then(() => {
                    toast(`Workflow has been updated successfully.`, { type: 'info' });
                    this.props.close(e);
                });
            } else {
                toast(res.error, { type: 'error' });
            }
        });
    };
    componentWillReceiveProps(props) {
        if (props.isOpen) {
            const workflow = props.workflow;
            this.setState({
                _id: props.workflow && props.workflow._id.$oid,
                workflow: {
                    ...this.state.workflow,
                    workflow_name: workflow.workflow_name,
                    description: workflow.description,
                },
            });
        }
    }
    render() {
        return (
            <Modal isOpen={this.props.isOpen} toggle={e => this.props.close(e)}>
                <Form onSubmit={this.onSubmit}>
                    <ModalHeader toggle={e => this.props.close(e)}>
                        <h3>Edit Workflow</h3>
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="workflow_name">Name</Label>
                            <Input
                                type="text"
                                id="workflow_name"
                                name="workflow_name"
                                value={this.state.workflow.workflow_name}
                                onChange={this.onChange}
                                required
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="description">Description</Label>
                            <Input
                                type="textarea"
                                name="description"
                                id="description"
                                value={this.state.workflow.description}
                                onChange={this.onChange}
                            />
                        </FormGroup>
                        {/* <FormGroup>
                            <Label for="extractors">Apps</Label>
                            {
                                this.props.workflowExtractors && this.props.workflowExtractors.join(", ")
                            }
                        </FormGroup> */}
                        <FormGroup>
                            <small>id: </small>
                            <code>{this.props.workflow && this.props.workflow._id.$oid}</code>
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" type="submit">
                            <TiArrowRepeat /> update
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
        );
    }
}

export default connect(null, { getAllWorkflows, updateWorkflow })(UpdateWorkflow);
