import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getAllWorkflows, getOneWorkflow, updateWorkflow } from '../../../actions/workflowActions';
import { Row, Col, Button, Card, CardTitle, CardText, CardBody } from 'reactstrap';
import { Loader } from '../../../vibe';
import { TiPlusOutline, TiTrash, TiChevronRightOutline, TiInfoLarge, TiEdit } from 'react-icons/ti';
import CreateWorkflow from './CreateWorkflow';
import DeleteWorkflow from './DeleteWorkflow';
import UpdateWorkflow from './UpdateWorkflow';

class WorkflowLists extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            createModal: false,
            deletModal: false,
            editModal: false,
            _id: '',
            toUpdateWorkflow: null,
        };
    }
    componentDidMount() {
        this.props.getAllWorkflows().then(() => {
            this.setState({
                loading: false,
            });
        });
    }
    openCreateModal = () => {
        this.setState({
            createModal: true,
        });
    };
    closeCreateModal = e => {
        e.preventDefault();
        this.setState({
            createModal: false,
        });
    };
    openDeleteModal = id => {
        this.setState({
            deletModal: true,
            _id: id,
        });
    };
    closeDeleteModal = e => {
        e.preventDefault();
        this.setState({
            deletModal: false,
        });
    };
    openEditModal = id => {
        this.props
            .getOneWorkflow(id)
            .then(data => {
                this.setState({
                    toUpdateWorkflow: data,
                    editModal: true,
                });
            })
            .catch(err => {
                console.error(err);
            });
    };
    closeEditModal = e => {
        this.setState({
            editModal: false,
        });
    };
    render() {
        return (
            <div className="scenarios">
                <CreateWorkflow isOpen={this.state.createModal} close={this.closeCreateModal} />
                <DeleteWorkflow
                    isOpen={this.state.deletModal}
                    close={this.closeDeleteModal}
                    id={this.state._id}
                />
                <UpdateWorkflow
                    isOpen={this.state.editModal}
                    close={this.closeEditModal}
                    workflow={this.state.toUpdateWorkflow}
                />
                <div className="create-workflow text-right mb-3">
                    <Button color="warning" onClick={this.openCreateModal}>
                        <TiPlusOutline /> Add Workflow
                    </Button>
                </div>
                <Row>
                    {!this.state.loading ? (
                        Array.isArray(this.props.workflows) && this.props.workflows.length ? (
                            this.props.workflows &&
                            this.props.workflows.map(item => {
                                return (
                                    <Col className="col-sm-12 col-md-6 col-xl-4">
                                        <Card key={item._id.$oid} className="workflow-card" body>
                                            <CardTitle>
                                                <TiChevronRightOutline /> {item.workflow_name}
                                            </CardTitle>
                                            <Row>
                                                <Col>
                                                    <h6>Description</h6>
                                                    <p>{item.description}</p>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col>
                                                    <h6>Apps</h6>
                                                    <div>
                                                        {item.extractors &&
                                                        item.extractors.length ? (
                                                            <div>
                                                                <ul>
                                                                    {item.extractors.map(
                                                                        extractor => {
                                                                            return (
                                                                                <li>{extractor}</li>
                                                                            );
                                                                        }
                                                                    )}
                                                                </ul>
                                                            </div>
                                                        ) : (
                                                            <div>No App</div>
                                                        )}
                                                    </div>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col>
                                                    <h6>Inputs</h6>
                                                    <div>
                                                        {item.inputs &&
                                                        item.inputs.length ? (
                                                            <div>
                                                                <ul>
                                                                    {item.inputs.map(
                                                                        input => {
                                                                            return (
                                                                                <li>{input}</li>
                                                                            );
                                                                        }
                                                                    )}
                                                                </ul>
                                                            </div>
                                                        ) : (
                                                            <div>No App</div>
                                                        )}
                                                    </div>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col className="text-right">
                                                    <Button
                                                        size="sm"
                                                        color="info"
                                                        onClick={e =>
                                                            this.openEditModal(item._id.$oid)
                                                        }
                                                    >
                                                        <TiEdit size="1.5em" />
                                                    </Button>{' '}
                                                    <Button
                                                        size="sm"
                                                        color="danger"
                                                        onClick={e =>
                                                            this.openDeleteModal(item._id.$oid)
                                                        }
                                                    >
                                                        <TiTrash size="1.5em" />
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </Card>
                                    </Col>
                                );
                            })
                        ) : (
                            <Col className="nothing-have-to-show">
                                <p>Have nothing to show!</p>
                            </Col>
                        )
                    ) : (
                        <Col className="loader-container">
                            <Loader type="puff" />
                        </Col>
                    )}
                </Row>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    workflows: state.workflowReducer.workflows,
    workflow: state.workflowReducer.workflow,
});

export default connect(mapStateToProps, { getAllWorkflows, getOneWorkflow, updateWorkflow })(
    WorkflowLists
);
