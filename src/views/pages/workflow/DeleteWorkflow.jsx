import React, { Component } from 'react';
import { Form, Button, Modal, ModalHeader, ModalBody, ModalFooter, Alert } from 'reactstrap';
import { TiPlusOutline, TiTimesOutline } from 'react-icons/ti';
import { connect } from 'react-redux';
import { deleteWorkflow, getAllWorkflows } from '../../../actions/workflowActions';
import { toast } from 'react-toastify';

export class CreateScenario extends Component {
    constructor(props) {
        super(props);
    }
    onSubmit = e => {
        e.preventDefault();
        let id = {
            _id: this.props.id,
        };
        this.props.deleteWorkflow(id).then(res => {
            if (res.status === 'OK') {
                this.props.getAllWorkflows().then(() => {
                    toast(`Workflow has been deleted successfully.`, { type: 'info' });
                    this.props.close(e);
                });
            } else {
                toast(res.error, { type: 'error' });
            }
        });
    };
    render() {
        return (
            <Modal isOpen={this.props.isOpen} toggle={e => this.props.close(e)}>
                <Form onSubmit={this.onSubmit}>
                    <ModalHeader toggle={e => this.props.close(e)}>
                        <h3>Delete Workflow</h3>
                    </ModalHeader>
                    <ModalBody>
                        <Alert color="danger">
                            You are trying to Delete Workflow!
                        </Alert>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" type="submit">
                            <TiPlusOutline /> Delete
                        </Button>{' '}
                        <Button color="warning" onClick={e => this.props.close(e)}>
                            <TiTimesOutline /> Cancel
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
        );
    }
}

export default connect(null, { deleteWorkflow, getAllWorkflows })(CreateScenario);
