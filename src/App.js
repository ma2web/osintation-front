import React, { Component } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import store from './store';
import './vibe/scss/styles.scss';
import Login from './views/pages/login/Login';
import DashboardLayout from './layouts/DashboardLayout';

export default class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let PrivateRoute = ({ component: Component, isLogin, ...rest }) => {
            return (
                <Route
                    {...rest}
                    render={props => {
                        let isLogin = localStorage.getItem('token') ? true : false;

                        if (isLogin) {
                            return <Component {...props} />;
                        } else {
                            return <Redirect to="/login" />;
                        }
                    }}
                />
            );
        };

        return (
            <Provider store={store}>
                <ToastContainer
                    position="bottom-right"
                    autoClose={3000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    rtl={false}
                    closeOnClick
                    pauseOnVisibilityChange
                    draggable
                    pauseOnHover
                />
                <BrowserRouter>
                    <Switch>
                        <Route exact path="/Login" component={Login} />
                        <PrivateRoute component={DashboardLayout} />
                    </Switch>
                </BrowserRouter>
            </Provider>
        );
    }
}
