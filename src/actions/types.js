export const FETCH_CASES = 'FETCH_CASES';
export const NEW_CASE = 'NEW_CASE';
export const LOGIN_SET_ME = 'LOGIN_SET_ME';
export const LOGIN_SET_TOKEN = 'LOGIN_SET_TOKEN';
export const LOGIN_SET_ERROR = 'LOGIN_SET_ERROR';

// workflow
export const GET_ALL_WORKFLOWS = 'GET_ALL_WORKFLOWS';
export const GET_ONE_WORKFLOW = 'GET_ONE_WORKFLOW';

// scenario
export const GET_ALL_SCENARIOS = 'GET_ALL_SCENARIOS';
export const GET_ONE_SCENARIO = 'GET_ONE_SCENARIO';
export const GET_REPORTS = 'GET_REPORTS';
export const GET_ONE_REPORT = 'GET_ONE_REPORT';

// proxies
export const GET_ALL_PROXIES = 'GET_ALL_PROXIES';