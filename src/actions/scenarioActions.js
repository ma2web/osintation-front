import { GET_ALL_SCENARIOS, GET_ONE_SCENARIO, GET_REPORTS, GET_ONE_REPORT } from './types';

export const getAllScenarios = () => dispatch => {
    return fetch(`/scenario`, {
        method: 'GET',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
    })
        .then(res => res.json())
        .then(data => {
            dispatch({
                type: GET_ALL_SCENARIOS,
                payload: data,
            });
            return data;
        });
};

export const createScenario = data => dispatch => {
    return fetch(`/scenario`, {
        method: 'POST',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json())
        .then(data => {
            return data;
        });
};

export const deleteScenario = data => dispatch => {
    return fetch(`/scenario`, {
        method: 'DELETE',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json())
        .then(data => {
            return data;
        });
};

export const getOneScenario = id => dispatch => {
    return fetch(`/scenario/${id}`, {
        method: 'GET',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
    })
        .then(res => res.json())
        .then(data => {
            dispatch({
                type: GET_ONE_SCENARIO,
                payload: data,
            });
            return data;
        });
};

export const updateScenario = data => dispatch => {
    console.log(data);
    return fetch(`/scenario`, {
        method: 'PUT',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json())
        .then(data => {
            return data;
        });
};

export const addWorkflow = data => dispatch => {
    console.log(data);
    return fetch(`/scenario/add_workflow`, {
        method: 'POST',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json())
        .then(data => {
            return data;
        });
};

export const removeWorkflow = data => dispatch => {
    console.log(data);
    return fetch(`/scenario/remove_workflow`, {
        method: 'POST',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json())
        .then(data => {
            return data;
        });
};

export const runScenarioWorkflow = data => dispatch => {
    console.log(data);
    return fetch(`/scenario/run_workflow`, {
        method: 'POST',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json())
        .then(data => {
            return data;
        });
};

export const pauseScenarioWorkflow = data => dispatch => {
    console.log(data);
    return fetch(`/scenario/pause_workflow`, {
        method: 'POST',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json())
        .then(data => {
            return data;
        });
};

export const reportScenarioWorkflow = data => dispatch => {
    console.log(data);
    return fetch(`/scenario/report_workflow`, {
        method: 'POST',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json())
        .then(data => {
            dispatch({
                type: GET_REPORTS,
                payload: data,
            });
            console.log(data)
            return data;
        });
};

export const getReportScenarioWorkflow = id => dispatch => {
    console.log(id)
    return fetch(`/scenario/report/${id}`, {
        method: 'GET',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
    })
        .then(res => {
            console.log(res)
            return res.json()
        })
        .then(data => {
            
            console.log(data);
            dispatch({
                type: GET_ONE_REPORT,
                payload: data,
            });
            return data;
        }).catch(err => {
            console.log(err)
        });
};
