import { LOGIN_SET_ME, LOGIN_SET_TOKEN, LOGIN_SET_ERROR } from './types';
import { toast } from "react-toastify";

export const login = (email, password) => dispatch => {
    let data = {
        email: email,
        password: password,
    };
    return fetch(`/user/login/`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
        },
        body: JSON.stringify(data),
    })
        .then(res => {
            if (res.status !== 200) {
                dispatch({
                    type: LOGIN_SET_ERROR,
                    payload: res.error,
                });
                throw res;
            }
            return res.json();
        })
        .then(data => {
            localStorage.setItem('token', data.access_token);
            dispatch({
                type: LOGIN_SET_ME,
                payload: data,
            });
            dispatch({
                type: LOGIN_SET_TOKEN,
                payload: data.access_token,
            });
            return data;
        })
        .catch(err => {
            dispatch({
                type: LOGIN_SET_ERROR,
                payload: {
                    status: err.status,
                    message: err.statusText,
                },
            });
        });
};
