import { GET_ALL_WORKFLOWS, GET_ONE_WORKFLOW } from './types';

export const getAllWorkflows = () => dispatch => {
    return fetch(`/workflow`, {
        method: 'GET',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
    })
        .then(res => res.json())
        .then(data => {
            dispatch({
                type: GET_ALL_WORKFLOWS,
                payload: data,
            });
            return data;
        });
};

export const createWorkflow = data => dispatch => {
    return fetch(`/workflow`, {
        method: 'POST',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json())
        .then(data => {
            return data;
        });
};

export const deleteWorkflow = data => dispatch => {
    return fetch(`/workflow`, {
        method: 'DELETE',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json())
        .then(data => {
            return data;
        });
};

export const getOneWorkflow = id => dispatch => {
    return fetch(`/workflow/${id}`, {
        method: 'GET',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
    })
        .then(res => res.json())
        .then(data => {
            dispatch({
                type: GET_ONE_WORKFLOW,
                payload: data,
            });
            return data;
        });
};

export const updateWorkflow = data => dispatch => {
    console.log(data);
    return fetch(`/workflow`, {
        method: 'PUT',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json())
        .then(data => {
            return data;
        });
};
