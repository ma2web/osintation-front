import { GET_ALL_PROXIES } from './types';

export const getAllProxies = () => dispatch => {
    return fetch(`/proxy/list`, {
        method: 'GET',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            AccessToken: `${localStorage.getItem('token')}`,
        },
    })
        .then(res => res.json())
        .then(data => {
            dispatch({
                type: GET_ALL_PROXIES,
                payload: data
            })
            return data
        });
};
