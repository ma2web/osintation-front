import { FETCH_CASES, NEW_CASE } from './types';

export const fetchCases = () => dispatch => {
  return fetch('https://jsonplaceholder.typicode.com/users')
    .then(res => res.json())
    .then(data => {
      dispatch({
        type: FETCH_CASES,
        payload: data,
      });
      return data
    });
};

export const createCase = caseData => dispatch => {
  console.log('action called');
  fetch('https://jsonplaceholder.typicode.com/users', {
    method: 'POST',
    headers: {
      'content-type': 'application/json',
    },
    body: JSON.stringify(caseData),
  })
    .then(res => res.json())
    .then(newCase => {
      console.log(newCase);
      dispatch({
        type: NEW_CASE,
        payload: {
          name: newCase.caseName,
          username: newCase.caseType,
          id: newCase.id,
        },
      });
    });
};
